# Marveler
**A Clean-Swift Project using Marvel API**

Project specifications :
- VIP (Clean-Swift) arhteture
- first-party CryptoKit as hash producer
- URLSessionTask as netwoker and a wrapper "WebService" as network layer
- CoreData as ORM and "DbManager" as wrapper
- dowloading images using native apis and also considered caching images
- nib files instead of storyboards.
- considered SOLID priciples  
- pagiantor manager
- and finally tried to use a minimal design for the app.

_ what's next_:
_if i want to make it a better sample project i will write some UnitTests and also i will use Combine as a first-party for throttling. ( beacuse of the lack of time posponed )_
