//
//  Container.swift
//  CustomNavBarTest
//
//  Created by Arash Goodarzi on 8/15/19.
//  Copyright © 2019 Arash Goodarzi. All rights reserved.
//

import Foundation

//MARK: Globals NameSapce
enum Global {
    enum Vars {
        
    }
    
    enum Funcs {
        
    }
    
    enum Constants {
        
    }
}

//MARK: Server Models
enum ServerModels {
    enum Response {
        
    }
    
    enum DTO {
        
    }
}

//MARK: HTTPRequestHelpers
enum HTTP {
    enum Heplers {
        
    }
}

//MARK: - Seeds ; fake data and test seeds
public enum Seed {
    
}
