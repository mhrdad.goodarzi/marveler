//
//  Seeds.swift
//  Marveler
//
//  Created by Arash on 12/8/21.
//

import Foundation

extension Seed {
    enum Main {
        static let thumbnail = ServerModels.Response.Thumbnail(path: "String",thumbnailExtension: "String")
        static let comics = ServerModels.Response.Comics(available: 0, collectionURI: "", items: [], returned: 0)
        static let stories = ServerModels.Response.Stories(available: 0, collectionURI: "", items: [], returned: 0)
        static let list: [ServerModels.Response.Result] = [
            ServerModels.Response.Result(id: 1,name: "",description:"",thumbnail: thumbnail, comics: comics, series: comics, events: comics, stories: stories)
        ]
        static let characters: ServerModels.Response.Characters = ServerModels.Response.Characters(data: ServerModels.Response.CharactersData(offset: 0, limit: 0, total: 0, count: 0, results: list))
    }
    
}
