//
//  Funcs.swift
//  CustomNavBarTest
//
//  Created by Arash Goodarzi on 8/15/19.
//  Copyright © 2019 Arash Goodarzi. All rights reserved.
//

import UIKit

typealias Closure = () -> ()
typealias ErrorClosure = (Error) -> ()

//Mark: - alert
extension Global.Funcs {
    static func showAlert(message: String?) {
        guard let message = message else {
            return
        }
        print(message)
    }
    
    static func showNoConnectionAlert() {
        print("no connection!")
    }
}

//MARK: - log
extension Global.Funcs {
    
    static func log(_ message: Any) {
        #if DEBUG
        print("*** \n \n","\(message)","\n \n ***")
        #endif
    }
    
    static func fullLog(_ message: Any, file: String = #file, function: String = #function, line: Int = #line) {
        #if DEBUG
        print("*** \n \n ","\(file) : \(function) - line: \(line)- \n \(message)","\n \n ***")
        #endif
    }
    

}



