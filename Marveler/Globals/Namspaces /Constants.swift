//
//  Constants.swift
//  CustomNavBarTest
//
//  Created by Arash Goodarzi on 8/22/19.
//  Copyright © 2019 Arash Goodarzi. All rights reserved.
//

import Foundation

extension Global.Constants {
    static let ThrottleInSeconds = 1
}
