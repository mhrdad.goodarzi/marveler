//
//  Mocks.swift
//  Marveler
//
//  Created by Arash on 12/8/21.
//


import Foundation


//MARK: WebServiceSuccessMock
class WebServiceSuccessMock: WebService {
    
    var seed: Codable?
    func request<T>(_ request: HTTP.Request<T>, completion: @escaping (Result<T, Error>) -> Void) -> URLSessionDataTask? {
        //FYI: T of request<T: Codable> type and WebServiceSuccessMock<T: Codable> are not the same
        guard let seed = seed as? T else {
            Global.Funcs.log("couldn't catch seed in WebServiceSuccessMock request")
            return nil
        }
        completion(.success(seed))
        return nil
    }
    
    func cancel(task: URLSessionDataTask?) {
        task?.cancel()
    }
}

//MARK: WebServiceFailureMock
class WebServiceFailureMock: WebService {
    
    let error: Error
    
    init(error: Error) {
        self.error = error
    }
    
    func request<T>(_ request: HTTP.Request<T>, completion: @escaping (Result<T, Error>) -> Void) -> URLSessionDataTask? {
        completion(.failure(error))
        return nil
    }
    
    func cancel(task: URLSessionDataTask?) {
        task?.cancel()
    }
}

