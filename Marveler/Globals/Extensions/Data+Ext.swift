//
//  Data.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//

import Foundation

extension Encodable {
  var serializedData: Data? {
    do {
      return try JSONEncoder().encode(self)
    } catch {
        Global.Funcs.log("Could not serializer server model.\n\(error.localizedDescription)")
      return nil
    }
  }
}

extension Data {
    
    func decodeTo<T: Decodable>(_ type: T.Type) -> T? {
        do {
            return try JSONDecoder().decode(T.self, from: self)
        } catch let error {
            Global.Funcs.log("Decoding Error to \(T.Type.self): \n \(error)")
            return nil
        }
    }
    
    func decodeToString() -> String? {
        let str = String(data: self, encoding: .utf8)
        return str
    }
    
}
