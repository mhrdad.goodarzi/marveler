//
//  String+Ext.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//

import Foundation

extension String {
    var isNotEmpty: Bool {
        return !self.isEmpty
    }
}

