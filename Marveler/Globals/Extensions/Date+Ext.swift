//
//  Date.swift
//  Marveler
//
//  Created by Arash on 11/27/21.
//

import Foundation

extension Date {
    static var currentTimestamp: String {
        let interval = String(Date().timeIntervalSince1970)
        return interval
    }
}
