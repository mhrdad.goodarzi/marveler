
import UIKit

extension UIImageView {
    
    //FYI: to persist cache on RAM
    private static var imageCache = NSCache<AnyObject, AnyObject>()
    private static var imageQueue = OperationQueue()
    private static var downloadBoxes: [DownloadBox] = []
    
    func downloadImage(url: String?, onSuccess: Closure? = nil) {
        guard let url = url else {
            Global.Funcs.log("url not provded in downloadImage")
            return
        }
        
        //cache check
        let downloadedImage = UIImageView.imageCache.object(forKey: url as AnyObject)
        guard (downloadedImage as? UIImage) == nil else {
            self.image = downloadedImage as? UIImage
            onSuccess?()
            return
        }
        
        //downloading
        let blockOperation = BlockOperation()
        blockOperation.addExecutionBlock({ [weak self] in
            guard let _url = URL(string: url) else {
                Global.Funcs.log("image downloader url didnt catch")
                return
            }
            do {
                let data = try Data(contentsOf: _url)
                guard let newImage = UIImage(data: data) else {
                    self?.image = #imageLiteral(resourceName: "placeholder")
                    onSuccess?()
                    return
                }
                UIImageView.imageCache.setObject(newImage, forKey: url as AnyObject)
                self?.runOnMainThread { [weak self] in
                    self?.image = newImage
                    onSuccess?()
                }
            } catch {
                Global.Funcs.log(error)
                DispatchQueue.main.async { [weak self] in
                    self?.image = nil
                }
            }
        })
        UIImageView.downloadBoxes.append(DownloadBox(imageView: self, block: blockOperation))
        UIImageView.imageQueue.addOperation(blockOperation)
    }
    
    fileprivate func runOnMainThread(block: @escaping Closure) {
        if Thread.isMainThread {
            block()
        } else {
            let mainQueue = OperationQueue.main
            mainQueue.addOperation({
                block()
            })
        }
    }
    
    func cancelPreviousDownloadAndShowPlaceholder() {
        //placeholder
        self.image = #imageLiteral(resourceName: "placeholder")
        let index: Int? = UIImageView.downloadBoxes.firstIndex(where: { $0.imageView === self })
        let anImageIsDownlingForTheImageView = index != nil
        if anImageIsDownlingForTheImageView {
            let index = index ?? 0
            UIImageView.downloadBoxes[index].block.cancel()
            UIImageView.downloadBoxes.remove(at: index)
        }
    }
}

fileprivate struct DownloadBox {
    let imageView: UIImageView
    let block: BlockOperation
}
