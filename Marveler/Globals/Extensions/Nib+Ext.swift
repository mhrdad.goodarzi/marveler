//
//  Nib.swift
//  Dericoin
//
//  Created by Arash on 1/5/21.
//  Copyright © 2021 Dericoin. All rights reserved.
//

import UIKit

//MARK: UIView
extension UIView {
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIView>() -> T {
            
            guard let view = Bundle.main.loadNibNamed(String(describing: T.self), owner: self, options: nil)?.first as? T else {
                fatalError("no nib with this name")
            }
            return view
        }
        
        return instantiateFromNib()
    }
}


//MARK: UIViewController
extension UIViewController {
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            let viewController = T(nibName: "\(self)", bundle: nil)
            return viewController
        }
        return instantiateFromNib()
    }
}
