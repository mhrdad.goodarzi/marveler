//
//  TableView+Ext.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//

import UIKit

extension UITableView {
    func register(list: [UITableViewCell.Type]) {
        list.forEach { self.register($0.self, forCellReuseIdentifier: $0.getReuseIdentifier) }
    }
    
    func registerFromNib(list: [UITableViewCell.Type]) {
        list.forEach { self.register(UINib(nibName: $0.getReuseIdentifier, bundle: nil), forCellReuseIdentifier: $0.getReuseIdentifier) }
    }
}

extension UITableViewCell {
    static var getReuseIdentifier: String {
        return "\(self)"
    }
}


extension UICollectionViewCell {
    static var getReuseIdentifier: String {
        return "\(self)"
    }
}

extension UICollectionView {
    func register(list: [UICollectionViewCell.Type]) {
        list.forEach { [weak self] in
            self?.register($0.self, forCellWithReuseIdentifier: $0.getReuseIdentifier)
        }
    }
    
    func registerFromNib(list: [UICollectionViewCell.Type]) {
        list.forEach { [weak self] in
            self?.register(UINib(nibName: $0.getReuseIdentifier, bundle: nil), forCellWithReuseIdentifier: $0.getReuseIdentifier)
        }
    }
}
