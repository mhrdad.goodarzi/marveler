//
//  FavoritesRouter.swift
//  Marveler
//
//  Created by Arash on 11/28/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

protocol FavoritesRoutingLogic: AnyObject {
    func routeToDetail()
}

protocol FavoritesDataPassing {
    var dataStore: FavoritesDataStore? { get }
}

class FavoritesRouter: NSObject, FavoritesRoutingLogic, FavoritesDataPassing {
    
    weak var viewController: FavoritesViewController?
    var dataStore: FavoritesDataStore?
    
    
    // MARK: routeToDetail
    func routeToDetail() {
        guard let sourceVC = viewController, let sourceDS = dataStore else {
            fatalError("Could not form source information!")
        }
        let destinationVC = DetailViewController.loadFromNib()
        guard var destinationDS = destinationVC.interactor else {
            return
        }
        passDataToDetail(source: sourceDS, destination: &destinationDS)
        navigateToDestination(source: sourceVC, destination: destinationVC)
    }

    private func passDataToDetail(source: FavoritesDataStore, destination: inout DetailBusinessLogic & DetailDataStore) {
        destination.selectedItem = source.selectedItem
    }

    private func navigateToDestination(source: FavoritesViewController, destination: DetailViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }

    
    //end of class
}
