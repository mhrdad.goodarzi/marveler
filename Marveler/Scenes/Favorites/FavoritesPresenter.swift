//
//  FavoritesPresenter.swift
//  Marveler
//
//  Created by Arash on 11/28/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

protocol FavoritesPresentationLogic: AnyObject {
    func presentPrepare(response: Favorites.Prepare.Response)
}

class FavoritesPresenter: FavoritesPresentationLogic {
    
    weak var viewController: FavoritesDisplayLogic?
    
    func presentPrepare(response: Favorites.Prepare.Response) {

        switch response.result {
        case .success(let list):
            
            let list = list.map { result -> PopulatedResult in
                let imagePath = result.thumbnail.path + "/portrait_medium" + "." + result.thumbnail.thumbnailExtension
                let populatedResult = PopulatedResult(id: String(result.id), name: result.name, description: result.description, imagePath: imagePath)
                return populatedResult
            }
            let viewModel = Favorites.Prepare.ViewModel.Success(list: list)
            viewController?.displaySuccessPrepare(viewModel: viewModel)
        case .failure(let error):
            let viewModel = Favorites.Prepare.ViewModel.Failure(message: error.localizedDescription)
            viewController?.displayErrorPrepare(viewModel: viewModel)
        }
    }
    //end of class
}
