//
//  FavoritesViewController.swift
//  Marveler
//
//  Created by Arash on 11/28/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.
//


import UIKit

protocol FavoritesDisplayLogic: AnyObject {
    func displaySuccessPrepare(viewModel: Favorites.Prepare.ViewModel.Success)
    func displayErrorPrepare(viewModel: Favorites.Prepare.ViewModel.Failure)
}

class FavoritesViewController: UIViewController {
    
    //MARK: Outlets and vars
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: TableView!
    var interactor: (FavoritesBusinessLogic & FavoritesDataStore)?
    private var list: [PopulatedResult]?
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var retryBtn: UIButton!
    
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    private func setup() {
        let viewController = self
        let dbManager = DbManagerImp()
        let interactor = FavoritesInteractor(dbManager: dbManager)
        let presenter = FavoritesPresenter()
        let router = FavoritesRouter()
        viewController.interactor = interactor
        interactor.presenter = presenter
        interactor.router = router
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepare()
    }
    
    //MARK: Actions
    @IBAction func retryBtnTapped(_ sender: Any) {
        prepare()
    }
    
    //MARK: Funcs
    func prepareUI() {
        prepareTableView()
        prepareNavBar()
    }
    
    func prepareNavBar() {
        self.navigationItem.title = "Favorites"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func prepareTableView() {
        tableView.registerFromNib(list: [ResultCell.self])
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func prepare() {
        indicator.startAnimating()
        let request = Favorites.Prepare.Request()
        interactor?.prepare(request: request)
    }
    
    //end of class
}

//MARK: - Extensions
extension FavoritesViewController: FavoritesDisplayLogic {
   
    func displaySuccessPrepare(viewModel: Favorites.Prepare.ViewModel.Success) {
        indicator.stopAnimating()
        list = viewModel.list
        hintLabel.isHidden = !viewModel.list.isEmpty
        tableView.reloadData()
        retryBtn.isHidden = true
    }
    
    func displayErrorPrepare(viewModel: Favorites.Prepare.ViewModel.Failure) {
        indicator.stopAnimating()
        Global.Funcs.showAlert(message: viewModel.message)
        retryBtn.isHidden = false
    }
}

extension FavoritesViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: ResultCell.getReuseIdentifier, for: indexPath) as? ResultCell,
            let result = list?[indexPath.row] else {
                return UITableViewCell()
            }
        cell.set(result: result)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let request = Favorites.DidSelectItem.Request(index: indexPath.row)
        interactor?.didSelectItem(request: request)
    }
}
