//
//  FavoritesModels.swift
//  Marveler
//
//  Created by Arash on 11/28/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

enum Favorites {
    
    enum Prepare {
        struct Request {
        }
        
        struct Response {
            let result: Result<[ServerModels.Response.Result],Error>
        }
        
        enum ViewModel {
            struct Success {
                let list: [PopulatedResult]
            }
            
            struct Failure {
                var message: String? = nil
            }
            
        }
    }
    
    enum ListReachedAt {
        struct Request {
            let index: Int
        }
    }
    
    enum DidSelectItem {
        struct Request {
            let index: Int
        }
    }
    
}
