//
//  FavoritesInteractor.swift
//  Marveler
//
//  Created by Arash on 11/28/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

protocol FavoritesBusinessLogic: AnyObject {
    func prepare(request: Favorites.Prepare.Request)
    func didSelectItem(request: Favorites.DidSelectItem.Request)
}

protocol FavoritesDataStore: AnyObject {
    var selectedItem: ServerModels.Response.Result? { get set }
}

class FavoritesInteractor: FavoritesBusinessLogic, FavoritesDataStore {
    
    var presenter: FavoritesPresentationLogic?
    var router: (FavoritesRoutingLogic & FavoritesDataPassing)?
    var selectedItem: ServerModels.Response.Result?
    private var list: [ServerModels.Response.Result] = []
    let dbManager: DbManager
    
    init(dbManager: DbManager) {
        self.dbManager = dbManager
    }
    
    func prepare(request: Favorites.Prepare.Request) {
       
        dbManager.fetchAllResults {  [weak self] results in
            let resp = Favorites.Prepare.Response(result: .success(results))
            self?.presenter?.presentPrepare(response: resp)
            self?.list = results
        } onFailure: { [weak self] error in
            let resp = Favorites.Prepare.Response(result: .failure(error))
            self?.presenter?.presentPrepare(response: resp)
        }
    }
    
    func didSelectItem(request: Favorites.DidSelectItem.Request) {
        let item = list[request.index]
        selectedItem = item
        router?.routeToDetail()
    }

    //end of class
}
