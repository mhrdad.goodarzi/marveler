//
//  MainListPresenter.swift
//  Marveler
//
//  Created by Arash on 11/25/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

protocol MainListPresentationLogic: AnyObject {
    func presentPrepare(response: MainList.Prepare.Response)
}

class MainListPresenter: MainListPresentationLogic {
    
    weak var viewController: MainListDisplayLogic?
    
    func presentPrepare(response: MainList.Prepare.Response) {

        switch response.result {
        case .success:
            
            let list = response.list.map { result -> PopulatedResult in
                let imagePath = result.thumbnail.path + "/portrait_medium" + "." + result.thumbnail.thumbnailExtension
                let populatedResult = PopulatedResult(id: String(result.id), name: result.name, description: result.description, imagePath: imagePath)
                return populatedResult
            }
            let viewModel = MainList.Prepare.ViewModel.Success(list: list)
            viewController?.displaySuccessPrepare(viewModel: viewModel)
        case .failure(let error):
            
            //No Connection
            if let error = error as? NoConnectionError {
                let viewModel = MainList.Prepare.ViewModel.Failure(message: error.localizedDescription)
                viewController?.displayNoConnectionPrepare(viewModel: viewModel)
                return
            }
            
            //Server Error
            let viewModel = MainList.Prepare.ViewModel.Failure(message: error.localizedDescription)
            viewController?.displayServerErrorPrepare(viewModel: viewModel)
        }
    }
    //end of class
}
