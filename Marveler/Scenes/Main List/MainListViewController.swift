//
//  MainListViewController.swift
//  Marveler
//
//  Created by Arash on 11/25/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.
//


import UIKit

protocol MainListDisplayLogic: AnyObject {
    func displaySuccessPrepare(viewModel: MainList.Prepare.ViewModel.Success)
    func displayServerErrorPrepare(viewModel: MainList.Prepare.ViewModel.Failure)
    func displayNoConnectionPrepare(viewModel: MainList.Prepare.ViewModel.Failure)
}

class MainListViewController: UIViewController {
    
    //MARK: Outlets and vars
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: TableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var interactor: (MainListBusinessLogic & MainListDataStore)?
    private var list: [PopulatedResult]?
    @IBOutlet weak var retryBtn: UIButton!
    
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    private func setup() {
        let viewController = self
        let webService = WebServcieImp()
        let auth = Auth()
        let interactor = MainListInteractor(webService: webService, auth: auth)
        let presenter = MainListPresenter()
        let router = MainListRouter()
        viewController.interactor = interactor
        interactor.presenter = presenter
        interactor.router = router
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        prepare()
    }
    
    //MARK: Actions
    @IBAction func retryBtnTapped(_ sender: Any) {
        prepare()
    }
    
    //MARK: Funcs
    func prepareUI() {
        prepareSearchBar()
        prepareTableView()
        prepareNavBar()
    }
    
    func prepare() {
        indicator.startAnimating()
        let request = MainList.Prepare.Request()
        interactor?.prepare(request: request)
    }
    
    func prepareNavBar() {
        self.navigationItem.title = "Marveler"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func prepareTableView() {
        tableView.registerFromNib(list: [ResultCell.self])
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func prepareSearchBar() {
        searchBar.searchTextField.textColor = .white
        searchBar.delegate = self
    }
    
    func searchKeyChanged(to key: String?) {
        indicator.startAnimating()
        let request = MainList.Search.Request(key: key)
        interactor?.searchKeyChanged(request: request)
    }
    
    //end of class
}

//MARK: - Extensions
extension MainListViewController: MainListDisplayLogic {
    
    func displaySuccessPrepare(viewModel: MainList.Prepare.ViewModel.Success) {
        indicator.stopAnimating()
        list = viewModel.list
        tableView.reloadData()
        retryBtn.isHidden = true
    }
    
    func displayServerErrorPrepare(viewModel: MainList.Prepare.ViewModel.Failure) {
        indicator.stopAnimating()
        Global.Funcs.showAlert(message: viewModel.message)
        retryBtn.isHidden = false
    }
    
    func displayNoConnectionPrepare(viewModel: MainList.Prepare.ViewModel.Failure) {
        indicator.stopAnimating()
        Global.Funcs.showNoConnectionAlert()
        retryBtn.isHidden = false
    }
}

extension MainListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: ResultCell.getReuseIdentifier, for: indexPath) as? ResultCell,
            let result = list?[indexPath.row] else {
                return UITableViewCell()
            }
        cell.set(result: result)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let request = MainList.DidSelectItem.Request(index: indexPath.row)
        interactor?.didSelectItem(request: request)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let request = MainList.ListReachedAt.Request(index: indexPath.row)
        let reachedAtBottom = interactor?.listReachedAt(request: request) ?? false
        if reachedAtBottom {
            indicator.startAnimating()
        }
    }
}

extension MainListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchKeyChanged(to: searchText)
    }
}
