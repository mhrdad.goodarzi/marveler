//
//  HeroCell.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//

import UIKit

class ResultCell: UITableViewCell {

    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var name: UILabel!
    
    func set(result: PopulatedResult) {
        idLabel.text = result.id
        name.text = result.name
        resultImageView.downloadImage(url: result.imagePath)
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        resultImageView.cancelPreviousDownloadAndShowPlaceholder()
        idLabel.text = "..."
        name.text = "..."
    }
}
