//
//  MainListHTTPHelpers.swift
//  Marveler
//
//  Created by Arash on 11/25/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


extension HTTP.Heplers {
    
    static func getCharacters(timeStamp: String,apikey: String, searchKey: String?, limit: Int, offset: Int, hash: String) -> HTTP.Request<ServerModels.Response.Characters> {
        let url: URLPath = .baseURL / .characters
        var params: [String: Any] = ["ts":timeStamp, "apikey": apikey,"hash": hash,"limit": limit,"offset":offset]
        if let searchKey = searchKey, searchKey.isNotEmpty {
            params["nameStartsWith"] = searchKey
        }
        let httpReq = HTTP.Request<ServerModels.Response.Characters>(method: .GET, url: url, parameters: params, bodyMessage: nil)
        return httpReq
    }
}
