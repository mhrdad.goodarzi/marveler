//
//  MainListModels.swift
//  Marveler
//
//  Created by Arash on 11/25/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

enum MainList {
    
    enum Prepare {
        struct Request {
        }
        
        struct Response {
            let result: Result<ServerModels.Response.Characters,Error>
            let list: [ServerModels.Response.Result]
        }
        
        enum ViewModel {
            struct Success {
                let list: [PopulatedResult]
            }
            
            struct Failure {
                var message: String? = nil
            }
            
        }
    }
    
    enum Search {
        struct Request {
            let key: String?
        }
    }
    
    enum ListReachedAt {
        struct Request {
            let index: Int
        }
    }
    
    enum DidSelectItem {
        struct Request {
            let index: Int
        }
    }
    
}

struct PopulatedResult {
    let id: String
    let name, description: String
    let imagePath: String?
}
