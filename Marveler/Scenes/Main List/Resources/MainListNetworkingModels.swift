//
//  MainListNetworkingModels.swift
//  Marveler
//
//  Created by Arash on 11/25/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import Foundation

extension ServerModels.Response {
    
    // MARK: - Characters
    struct Characters: Codable {
        let data: CharactersData
    }

    // MARK: - DataClass
    struct CharactersData: Codable {
        let offset, limit, total, count: Int
        let results: [Result]
    }

    // MARK: - Result
    struct Result: Codable, Equatable {
        static func == (lhs: ServerModels.Response.Result, rhs: ServerModels.Response.Result) -> Bool {
            lhs.id == rhs.id
        }
        let id: Int
        let name, description: String
        let thumbnail: Thumbnail
        let comics, series, events: Comics
        let stories: Stories
    }
    
    // MARK: - Comics
    struct Comics: Codable {
        let available: Int
        let collectionURI: String
        let items: [ComicsItem]
        let returned: Int
    }

    // MARK: - ComicsItem
    struct ComicsItem: Codable {
        let resourceURI: String
        let name: String
    }

    // MARK: - Stories
    struct Stories: Codable {
        let available: Int
        let collectionURI: String
        let items: [StoriesItem]
        let returned: Int
    }

    // MARK: - StoriesItem
    struct StoriesItem: Codable {
        let resourceURI: String
        let name: String
        let type: String
    }

    // MARK: - Thumbnail
    struct Thumbnail: Codable {
        let path: String
        let thumbnailExtension: String

        enum CodingKeys: String, CodingKey {
            case path
            case thumbnailExtension = "extension"
        }
    }

    // MARK: - URLElement
    struct URLElement: Codable {
        let type: String
        let url: String
    }


}


