//
//  MainListInteractor.swift
//  Marveler
//
//  Created by Arash on 11/25/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

protocol MainListBusinessLogic: AnyObject {
    func prepare(request: MainList.Prepare.Request)
    func didSelectItem(request: MainList.DidSelectItem.Request)
    func searchKeyChanged(request: MainList.Search.Request)
    func listReachedAt(request: MainList.ListReachedAt.Request) -> Bool
}

protocol MainListDataStore: AnyObject {
    var selectedItem: ServerModels.Response.Result? { get set }
}

class MainListInteractor: MainListBusinessLogic, MainListDataStore {
    
    var presenter: MainListPresentationLogic?
    var router: (MainListRoutingLogic & MainListDataPassing)?
    private var searchTask: URLSessionDataTask?
    let webService: WebService
    var selectedItem: ServerModels.Response.Result?
    private var list: [ServerModels.Response.Result] = []
    private var paginator = Paginator()
    private let hasher = MarvelHasher()
    let auth: Auth
    private var searchKey: String?
    
    init(webService: WebService, auth: Auth) {
        self.webService = webService
        self.auth = auth
        setupPaginator()
    }
    
    private func setupPaginator() {
        paginator.pageSize = 20
        paginator.onNextPage { [weak self] _ in
            self?.requestList()
        }
    }
    
    private func requestList() {
        let timeStamp = Date.currentTimestamp
        let hash = hasher.generate(timeStamp: timeStamp, privateKey: auth.privateKey, publicKey: auth.publicKey)
        let httpRequest = HTTP.Heplers.getCharacters(timeStamp: timeStamp, apikey: auth.publicKey, searchKey: searchKey, limit: paginator.pageSize, offset: paginator.offset, hash: hash)
        searchTask = webService.request(httpRequest) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let serverResponse):
                let list = serverResponse.data.results
                self.paginator.update(by: list, totalResults: serverResponse.data.total)
                self.list += list
            case .failure:
                break
            }
            let resp = MainList.Prepare.Response(result: result, list: self.list)
            self.presenter?.presentPrepare(response: resp)
        }
    }
    
    func prepare(request: MainList.Prepare.Request) {
        requestList()
    }
    
    func listReachedAt(request: MainList.ListReachedAt.Request) -> Bool {
        paginator.listReached(at: request.index)
    }
    
    func searchKeyChanged(request: MainList.Search.Request) {
        webService.cancel(task: searchTask)
        searchKey = request.key
        list = []
        paginator.reset()
        requestList()
    }
    
    func didSelectItem(request: MainList.DidSelectItem.Request) {
        let item = list[request.index]
        selectedItem = item
        router?.routeToDetail()
    }
    
    //end of class
}
