//
//  MainListRouter.swift
//  Marveler
//
//  Created by Arash on 11/25/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

protocol MainListRoutingLogic: AnyObject {
    func routeToDetail()
}

protocol MainListDataPassing {
    var dataStore: MainListDataStore? { get }
}

class MainListRouter: NSObject, MainListRoutingLogic, MainListDataPassing {
    
    weak var viewController: MainListViewController?
    var dataStore: MainListDataStore?
    
    
    // MARK: routeToDetail
    func routeToDetail() {
        guard let sourceVC = viewController, let sourceDS = dataStore else {
            fatalError("Could not form source information!")
        }
        let destinationVC = DetailViewController.loadFromNib()
        guard var destinationDS = destinationVC.interactor else {
            return
        }
        passDataToDetail(source: sourceDS, destination: &destinationDS)
        navigateToDestination(source: sourceVC, destination: destinationVC)
    }

    private func passDataToDetail(source: MainListDataStore, destination: inout DetailBusinessLogic & DetailDataStore) {
        destination.selectedItem = source.selectedItem
    }

    private func navigateToDestination(source: MainListViewController, destination: DetailViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }

    
    //end of class
}
