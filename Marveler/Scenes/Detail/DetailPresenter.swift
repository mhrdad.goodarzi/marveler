//
//  DetailPresenter.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

protocol DetailPresentationLogic: AnyObject {
    func presentPrepare(response: Detail.Prepare.Response)
    func presentFavorteBtnTapped(response: Detail.FavorteBtnTapped.Response)
}

class DetailPresenter: DetailPresentationLogic {
    
    weak var viewController: DetailDisplayLogic?
    
    func presentPrepare(response: Detail.Prepare.Response) {
        let imagePath = response.result.thumbnail.path + "/portrait_medium" + "." + response.result.thumbnail.thumbnailExtension
        let comics = response.result.comics.items.prefix(3).map { Banner(name: $0.name) }
        let events = response.result.events.items.prefix(3).map { Banner(name: $0.name) }
        let stories = response.result.stories.items.prefix(3).map { Banner(name: $0.name) }
        let series = response.result.series.items.prefix(3).map { Banner(name: $0.name) }
        let detail = PopulatedResultDetail(id: String(response.result.id), name: response.result.name, description: response.result.description, imagePath:imagePath, series: series, comics: comics, events: events, stories: stories)
        let viewModel = Detail.Prepare.ViewModel(detail: detail, isAddedToFavorites: response.isAddedToFavorites)
        viewController?.displayPrepare(viewModel: viewModel)
    }
    
    func presentFavorteBtnTapped(response: Detail.FavorteBtnTapped.Response) {
        var _isAddedToFavorites: Bool
        switch response.result {
        case .success(let isAddedToFavorites):
            _isAddedToFavorites = isAddedToFavorites
        case .failure:
            _isAddedToFavorites = false
        }
        let viewModel = Detail.FavorteBtnTapped.ViewModel(isAddedToFavorites: _isAddedToFavorites)
        viewController?.displayFavorteBtnTapped(viewModel: viewModel)
    }
    //end of class
}
