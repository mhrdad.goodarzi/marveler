//
//  DetailModels.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

enum Detail {
    
    enum Prepare {
        struct Request {
            
        }
        
        struct Response {
            let result: ServerModels.Response.Result
            let isAddedToFavorites: Bool
        }
        
        struct ViewModel {
            let detail: PopulatedResultDetail
            let isAddedToFavorites: Bool
            
        }
    }
    
    enum FavorteBtnTapped {
        struct Request {
            
        }
        
        struct Response {
            let result: Result<Bool,Error>
        }
        
        struct ViewModel {
            let isAddedToFavorites: Bool
        }
    }
}


struct PopulatedResultDetail {
    let id: String
    let name, description: String
    let imagePath: String?
    let series, comics, events,stories: [Banner]
    let isAddedToFavorites: Bool = false
}

struct Banner {
    //let imagePath: String?
    let name: String
}
