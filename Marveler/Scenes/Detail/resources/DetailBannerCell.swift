//
//  DetailBannerCell.swift
//  Marveler
//
//  Created by Arash on 11/28/21.
//

import UIKit

class DetailBannerCell: UICollectionViewCell {

    @IBOutlet weak var _name: UILabel!
    
    func set(banner: Banner) {
        _name.text = banner.name
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 0.5
    }
}
