//
//  DetailRouter.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

protocol DetailRoutingLogic: AnyObject {
    //func routeToZZZ()
}

protocol DetailDataPassing {
    var dataStore: DetailDataStore? { get }
}

class DetailRouter: NSObject, DetailRoutingLogic, DetailDataPassing {
    
    weak var viewController: DetailViewController?
    weak var dataStore: DetailDataStore?

    //end of class
}
