//
//  DetailViewController.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.
//


import UIKit

protocol DetailDisplayLogic: AnyObject {
    func displayPrepare(viewModel: Detail.Prepare.ViewModel)
    func displayFavorteBtnTapped(viewModel: Detail.FavorteBtnTapped.ViewModel)
}

class DetailViewController: UIViewController {
    
    //MARK: Outlets and vars
    var interactor: (DetailBusinessLogic & DetailDataStore)?
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var _description: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var _id: UILabel!
    @IBOutlet weak var comicsCollectionView: UICollectionView!
    @IBOutlet weak var eventsCollectionView: UICollectionView!
    @IBOutlet weak var storiesCollectionView: UICollectionView!
    @IBOutlet weak var seriesCollectionView: UICollectionView!
    @IBOutlet weak var favoriteBtn: UIButton!
    private var comics,events,stories,series: [Banner]?
    
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    private func setup() {
        let viewController = self
        let dbManager = DbManagerImp()
        let interactor = DetailInteractor(dbManager: dbManager)
        let presenter = DetailPresenter()
        let router = DetailRouter()
        viewController.interactor = interactor
        interactor.presenter = presenter
        interactor.router = router
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepare()
    }
    
    //MARK: Actions
    @IBAction func favoriteBtnTapped() {
        let requset = Detail.FavorteBtnTapped.Request()
        interactor?.favoriteBtnTApped(request: requset)
    }
    
    //MARK: Funcs
    func prepareUI() {
        imageView.layer.cornerRadius = imageView.frame.height / 2
        imageView.clipsToBounds = true
        prepareCollectionView()
    }
 
    func prepareCollectionView() {
        [comicsCollectionView,eventsCollectionView,storiesCollectionView,seriesCollectionView].forEach { [weak self] in
            $0?.registerFromNib(list: [DetailBannerCell.self])
            $0?.delegate = self
            $0?.dataSource = self
        }
    }
    
    func prepare() {
        let requset = Detail.Prepare.Request()
        interactor?.prepare(request: requset)
    }
    
    
    
    //end of class
}

//MARK: - Extensions
extension DetailViewController: DetailDisplayLogic {
    
    func displayPrepare(viewModel: Detail.Prepare.ViewModel) {
        _id.text = viewModel.detail.id
        name.text = viewModel.detail.name
        _description.text = viewModel.detail.description
        imageView.downloadImage(url: viewModel.detail.imagePath)
        comics = viewModel.detail.comics
        events = viewModel.detail.events
        stories = viewModel.detail.stories
        series = viewModel.detail.series
        [comicsCollectionView,eventsCollectionView,storiesCollectionView,seriesCollectionView].forEach {  $0?.reloadData() }
        let tintColor: UIColor = viewModel.isAddedToFavorites ? .yellow : .white
        favoriteBtn.tintColor = tintColor
        
    }
    
    func displayFavorteBtnTapped(viewModel: Detail.FavorteBtnTapped.ViewModel) {
        let tintColor: UIColor = viewModel.isAddedToFavorites ? .yellow : .white
        favoriteBtn.tintColor = tintColor
    }
}

extension DetailViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        switch collectionView {
        case comicsCollectionView:
            count = comics?.count ?? 0
        case eventsCollectionView:
            count = events?.count ?? 0
        case storiesCollectionView:
            count = stories?.count ?? 0
        case seriesCollectionView:
            count = series?.count ?? 0
        default:
            return 0
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailBannerCell.getReuseIdentifier, for: indexPath) as? DetailBannerCell
        var banner: Banner?
        switch collectionView {
        case comicsCollectionView:
            banner = comics?[indexPath.row]
        case eventsCollectionView:
            banner = events?[indexPath.row]
        case storiesCollectionView:
            banner = stories?[indexPath.row]
        case seriesCollectionView:
            banner = series?[indexPath.row]
        default:
            return UICollectionViewCell()
        }
        if let banner = banner {
            cell?.set(banner: banner)
        }
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
}
