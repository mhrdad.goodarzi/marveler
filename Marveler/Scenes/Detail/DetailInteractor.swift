//
//  DetailInteractor.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//  Copyright (c) 2021 Arash Goodarzi. All rights reserved.


import UIKit

protocol DetailBusinessLogic: AnyObject {
    func prepare(request: Detail.Prepare.Request)
    func favoriteBtnTApped(request: Detail.FavorteBtnTapped.Request)
}

protocol DetailDataStore: AnyObject {
    var selectedItem: ServerModels.Response.Result? { get set }
}

class DetailInteractor: DetailBusinessLogic, DetailDataStore {
    
    var presenter: DetailPresentationLogic?
    var router: (DetailRoutingLogic & DetailDataPassing)?
    var selectedItem: ServerModels.Response.Result?
    let dbManager: DbManager
    
    init(dbManager: DbManager) {
        self.dbManager = dbManager
    }
    
    func prepare(request: Detail.Prepare.Request) {
        guard let selectedItem = selectedItem else {
            return
        }
        dbManager.isExisted(result: selectedItem) { [weak self] isExisted in
            let response = Detail.Prepare.Response(result: selectedItem, isAddedToFavorites: isExisted)
            self?.presenter?.presentPrepare(response: response)
        } onFailure: { [weak self] error in
            let response = Detail.Prepare.Response(result: selectedItem, isAddedToFavorites: false)
            self?.presenter?.presentPrepare(response: response)
        }
        
    }
    
    func favoriteBtnTApped(request: Detail.FavorteBtnTapped.Request) {
        guard let selectedItem = selectedItem else {
            return
        }
        dbManager.isExisted(result: selectedItem) { [weak self] isExisted in
            if !isExisted {
                //Add
                self?.dbManager.add(result: selectedItem) { [weak self] in
                    let response = Detail.FavorteBtnTapped.Response(result: Result.success(true))
                    self?.presenter?.presentFavorteBtnTapped(response: response)
                } onFailure: { [weak self] error in
                    let response = Detail.FavorteBtnTapped.Response(result: .failure(error))
                    self?.presenter?.presentFavorteBtnTapped(response: response)
                }
            } else {
                //Remove
                self?.dbManager.remove(result: selectedItem, onSuccess: {
                    let response = Detail.FavorteBtnTapped.Response(result: Result.success(false))
                    self?.presenter?.presentFavorteBtnTapped(response: response)
                }, onFailure: { [weak self] error in
                    let response = Detail.FavorteBtnTapped.Response(result: .failure(error))
                    self?.presenter?.presentFavorteBtnTapped(response: response)
                })
            }
            
        } onFailure: { [weak self] error in
                let response = Detail.FavorteBtnTapped.Response(result: .failure(error))
                self?.presenter?.presentFavorteBtnTapped(response: response)
        }
    }
    
    //end of class
}
