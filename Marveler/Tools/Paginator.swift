//
//  Paginator.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//

import Foundation

struct Paginator {
    
    private var count: Int = 0
    var pageSize: Int = 20
    private var onNextPageListener: ((_ offset: Int) -> Void)?
    private var totalResults = 0
    private var hasNextPage: Bool {
        return count < totalResults
    }
    var currentPage: Int {
        let calculatedPage = (Double(count) / Double(pageSize)).rounded(.up)
        let currentPage = calculatedPage == 0 ? 1 : calculatedPage
        return Int(currentPage)
    }
    
    var offset: Int {
        let _offset = count == 0 ? 0 : currentPage * pageSize
        let offset = _offset > count ? count : _offset
        return offset
    }
    
    init() {
        
    }
    
    init(list: [Any], pageSize: Int) {
        self.count = list.count
        self.pageSize = pageSize
    }
    
    mutating func update(by list: [Any], totalResults: Int) {
        //update list
        if count == 0 {
            count = list.count
        } else {
            count += list.count
        }
        self.totalResults = totalResults
    }
    
    mutating func reset() {
        count = 0
    }
  
    func listReached(at index: Int) -> Bool {
        let reachedAtBottom = index == count - 1 && hasNextPage
        if reachedAtBottom { // last row
            onNextPageListener?(offset)
        }
        return reachedAtBottom
    }
    
    mutating func onNextPage(_ onNextPage: ((_ offset: Int) -> Void)?) {
        onNextPageListener = onNextPage
    }
}
