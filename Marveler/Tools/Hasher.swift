//
//  Hasher.swift
//  Marveler
//
//  Created by Arash on 11/26/21.
//

import Foundation
import CryptoKit

struct MarvelHasher {
    
    private func getHash(source: String) -> String {
        return Insecure.MD5.hash(data: source.data(using: .utf8)!).map { String(format: "%02hhx", $0) }.joined()
    }
    
    func generate(timeStamp: String, privateKey: String, publicKey: String) -> String {
        let source = timeStamp + privateKey + publicKey
        let hashValue = getHash(source: source)
        return hashValue
    }
}

