//
//  DbManager.swift
//  Marveler
//
//  Created by Arash on 11/28/21.
//

import UIKit
import CoreData

protocol DbManager {
    func fetchAllResults( onSuccess: ([ServerModels.Response.Result])->Void, onFailure: ErrorClosure?)
    func isExisted(result: ServerModels.Response.Result, onSuccess: (Bool)->Void, onFailure: ErrorClosure?)
    func add(result: ServerModels.Response.Result, onSuccess: Closure, onFailure: ErrorClosure?)
    func remove(result: ServerModels.Response.Result, onSuccess: Closure, onFailure: ErrorClosure?)
}


class DbManagerImp: DbManager {
    
    let entityName = "Character"
    private var context: NSManagedObjectContext {
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else {
            fatalError("context not available")
        }
        let context = appDel.persistentContainer.viewContext
        return context
    }
    
    func fetchAllResults( onSuccess: ([ServerModels.Response.Result])->Void, onFailure: ErrorClosure?) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        do  {
            guard let characters = try context.fetch(fetchRequest) as? [Character] else {
                let error = CustomError(message: "fetching failed!")
                onFailure?(error)
                return
            }
            let resutls = characters.compactMap { $0.resultModel?.decodeTo(ServerModels.Response.Result.self) }
            onSuccess(resutls)
        } catch {
            onFailure?(error)
        }
    }
    
    func isExisted(result: ServerModels.Response.Result, onSuccess: (Bool)->Void, onFailure: ErrorClosure?) {
        fetchAllResults { results in
            let isExisted = results.contains(result)
            onSuccess(isExisted)
        } onFailure: { error in
            onFailure?(error)
        }

    }
    
    func add(result: ServerModels.Response.Result, onSuccess: Closure, onFailure: ErrorClosure?) {
        let data = result.serializedData
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)!
        let character = Character(entity: entity, insertInto: context)
        character.resultModel = data
        character.id = Int32(result.id)
        context.insert(character)
        do {
            try context.save()
            onSuccess()
        } catch {
            onFailure?(error)
        }
    }
    
    func remove(result: ServerModels.Response.Result, onSuccess: Closure, onFailure: ErrorClosure?) {
        
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        do {
            guard let character = try context.fetch(fetchReq) as? [Character], !character.isEmpty else {
                let error = CustomError(message: "fetching failed!")
                onFailure?(error)
                return
            }
            for character in character {
                if let id = character.value(forKey: "id") as? Int32, id == result.id {
                    context.delete(character)
                    do {
                        try context.save()
                        onSuccess()
                        break
                    } catch {
                        let error = CustomError(message: "could not delete the character \(id)")
                        onFailure?(error)
                    }
                }
            }
        } catch {
            onFailure?(error)
        }
    }
    
}
