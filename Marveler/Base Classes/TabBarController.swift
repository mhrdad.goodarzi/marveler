//
//  TabBarController.swift
//  Marveler
//
//  Created by Arash on 11/27/21.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVCs()
        UINavigationBar.appearance().barTintColor = .white
        UITabBar.appearance().tintColor = .white
        tabBar.barTintColor = UIColor.navBarColor
    }
    
    private func setupVCs() {
        viewControllers = [
            createNavController(for: MainListViewController.loadFromNib(), title: "Main", image: #imageLiteral(resourceName: "list")),
            createNavController(for: FavoritesViewController.loadFromNib(), title: "Favorites", image: #imageLiteral(resourceName: "favorites"))
        ]
    }
    
    private func createNavController(for rootViewController: UIViewController, title: String, image: UIImage) -> UIViewController {
        let navController = NavigationController(rootViewController: rootViewController)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = image
        rootViewController.navigationItem.title = title
        return navController
    }
    
}
