//
//  TableView.swift
//  Marveler
//
//  Created by Arash on 11/27/21.
//

import UIKit

class TableView: UITableView {
    
    private var onListReachedAtBottomClosure: Closure?
    private var onDidScrollClosure: ((_ offset: CGPoint) -> Void)?
    
    func onListReachedAtBottom(_ completion: Closure?) {
        onListReachedAtBottomClosure = completion
    }
    func onDidScroll(_ completion: ((_ offset: CGPoint) -> Void)?) {
        onDidScrollClosure = completion
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let reachedAtBottom = (scrollView.contentOffset.y + scrollView.frame.size.height) + 30 >= scrollView.contentSize.height
        guard reachedAtBottom else {
            return
        }
        DispatchQueue.main.async { [weak self] in
            self?.onListReachedAtBottomClosure?()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            self.onListReachedAtBottomClosure?()
        }
    }
}

