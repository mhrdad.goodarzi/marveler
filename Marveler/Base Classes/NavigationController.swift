//
//  NavigationController.swift
//  Marveler
//
//  Created by Arash on 11/28/21.
//

import UIKit

class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.prefersLargeTitles = true
        let barAppearance = UINavigationBarAppearance()
            barAppearance.backgroundColor = UIColor.navBarColor
        barAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        barAppearance.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationBar.standardAppearance = barAppearance
        navigationBar.scrollEdgeAppearance = barAppearance
        navigationBar.barTintColor = UIColor.navBarColor
        navigationItem.standardAppearance = barAppearance
        navigationItem.scrollEdgeAppearance = barAppearance
        navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.red]
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.red]
        UINavigationBar.appearance().tintColor = .white

    }
    



}
