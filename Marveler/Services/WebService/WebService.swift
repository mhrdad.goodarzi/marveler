//
//  DataService.swift
//  CustomNavBarTest
//
//  Created by Arash Goodarzi on 8/15/19.
//  Copyright © 2019 Arash Goodarzi. All rights reserved.
//

import Foundation

//MARK: - Web Service
protocol WebService {
   
    @discardableResult func request<T: Codable>(_ request: HTTP.Request<T>, completion: @escaping (_ result: Result<T,Error>) -> Void ) -> URLSessionDataTask?
    func cancel(task: URLSessionDataTask?)
}

//MARK: - Web Service Implementation
class WebServcieImp: WebService {
    
    //MARK: Request
    @discardableResult
    func request<T: Codable>(_ request: HTTP.Request<T>, completion: @escaping (_ result: Result<T,Error>) -> Void ) -> URLSessionDataTask? {
        
        guard let urlRequest = request.nsURLRequestValue as URLRequest? else {
            let message = "Request is not valid!"
            let error = CustomError(message: message)
            DispatchQueue.main.async {
                completion(.failure(error))
            }
            return nil
        }
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: urlRequest) { responseData, urlResponse, err -> Void in
            
            if let netError = err {
                DispatchQueue.main.async {
                    completion(.failure(netError))
                }
                Global.Funcs.log(netError.localizedDescription)
                return
            }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let message = "httpResponse didn't catch!"
                Global.Funcs.log(message)
                let error = NoConnectionError()
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
                return
            }
        
            guard request.validStatusCodes.contains(httpResponse.statusCode) else {
                let message = "not valid status code!"
                Global.Funcs.log(message)
                let error = NoConnectionError()
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
                return
            }
            
            guard var responseData = responseData else {
                let error = NoConnectionError()
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
                return
            }
            
            // Parse Response
            do {
                if responseData.isEmpty || T.self == ServerModels.EmptyModel.self {
                    responseData = "{}".data(using: .utf8) ?? Data()
                }
                
                let responseObject = try JSONDecoder().decode(T.self, from: responseData)
                DispatchQueue.main.async {
                    completion(.success(responseObject))
                }
            } catch {
                Global.Funcs.log("Could not parse object of type: [\(type(of: T.self))]\n\(dump(error))")
            }
            
        }
        dataTask.resume()
        return dataTask
    }
    
    func cancel(task: URLSessionDataTask?) {
        task?.cancel()
        //And extra possible things to do ...
    }
    
}
