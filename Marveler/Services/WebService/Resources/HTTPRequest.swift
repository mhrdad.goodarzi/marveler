//
//  RequestModel.swift
//  CustomNavBarTest
//
//  Created by Arash Goodarzi on 8/15/19.
//  Copyright © 2019 Arash Goodarzi. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String: Any]


extension HTTP {
    struct Request<T: Codable> {
        
        let httpMethod: HTTP.Method
        let url: URLPath
        let responseType: T.Type
        let timeout: HTTP.TimeOut
        let parameters: JSONDictionary?
        let bodyMessage: Codable?
        let serializedBody: Data?
        let headers: [String: String]?
        let accept: HTTP.ContentType
        let contentType: HTTP.ContentType
        let validStatusCodes: ClosedRange<Int>
        
        init(method: HTTP.Method, url: URLPath, parameters: JSONDictionary? = nil, bodyMessage: Codable? = nil, serializedBody: Data? = nil, headers: [String: String]? = nil, timeOut: HTTP.TimeOut = .normal, acceptType: HTTP.ContentType = .json, contentType: HTTP.ContentType = .json, validStatusCodes: ClosedRange<Int> = 200...300) {
            self.httpMethod = method
            self.url = url
            self.timeout = timeOut
            self.parameters = parameters
            self.headers = headers
            self.accept = acceptType
            self.contentType = contentType
            self.bodyMessage = bodyMessage
            self.serializedBody = serializedBody
            self.responseType = T.self
            self.validStatusCodes = validStatusCodes
        }
        
        var nsURLRequestValue: NSURLRequest? {
            
            guard let url = url.getURL else {
                return nil
            }
            
            let result = NSMutableURLRequest(url: url as URL, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: self.timeout.rawValue)
            
            result.prepareRequestMethod(method: self.httpMethod)
            if let headers = self.headers {
                result.prepareHeaders(headers: headers)
            }
            result.prepareAcceptHeader(accept: self.accept)
            result.prepareContentTypeHeader(contentType: self.contentType)
            
            if self.httpMethod == HTTP.Method.GET || self.httpMethod == HTTP.Method.DELETE {
                if let params = self.parameters {
                    result.prepareURLParameters(params: params, url: url as NSURL)
                }
            } else {
                if let params = self.parameters {
                    if self.contentType == .urlEncodedForm {
                        result.prepareURLEndodedForm(params: params)
                    } else {
                        result.prepareURLParameters(params: params, url: url as NSURL)
                    }
                }
                if let bodyMessage = self.bodyMessage {
                    result.prepareBody(bodyMessage: bodyMessage)
                }
            }
            
            return result
        }
    }

}

extension NSMutableURLRequest {
    
    private func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")
        
        return string
        
            .addingPercentEncoding(withAllowedCharacters: characterSet)?
            .replacingOccurrences(of: " ", with: "+")
            .replacingOccurrences(of: " ", with: "+", options: [], range: nil) ?? ""
    }
    
    func prepareRequestMethod(method: HTTP.Method) {
        self.httpMethod = method.rawValue
    }
    
    func prepareHeaders(headers: [String: String]) {
        for key in headers.keys {
            self.setValue(headers[key], forHTTPHeaderField: key)
        }
    }
    
    func prepareAcceptHeader(accept: HTTP.ContentType) {
        self.setValue(accept.value, forHTTPHeaderField: HTTP.Headers.Accept)
    }
    
    func prepareContentTypeHeader(contentType: HTTP.ContentType) {
        self.setValue(contentType.value, forHTTPHeaderField: HTTP.Headers.ContentType)
    }
    
    func prepareURLEndodedForm(params: JSONDictionary) {
        let parameterArray = params.map { arg -> String in
            let (key, value) = arg
            let val = "\(value)"
            return "\(key)=\(self.percentEscapeString(val))"
        }
        self.httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
        
    }
    
    func prepareURLParameters(params: JSONDictionary, url: NSURL) {
        var qString = ""
        for key in params.keys {
            
            guard let val = params[key] else {
                Global.Funcs.log("Skipping value for key: \(key)")
                continue
            }
            qString += qString.isEmpty ? "" : "&"
            qString += key + "=" + String(describing: val)
        }
        
        if let encodedQuery = qString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            qString = encodedQuery
        }
        
        guard let absoluteString = url.absoluteString else {
            return
        }
        let newUrlStr = absoluteString + "?" + qString
        guard let newURL = NSURL(string: newUrlStr) else {
            Global.Funcs.log("Invalid url: \(newUrlStr)")
            return
        }
        
        self.url = newURL as URL
    }
    
    func prepareBody(bodyMessage: Codable) {
        guard let bodyData = bodyMessage.serializedData else {
            Global.Funcs.log("Body message is empty. Skipping body value set.")
            return
        }
        self.httpBody = bodyData
        self.setValue("\(bodyData.count)", forHTTPHeaderField: HTTP.Headers.ContentLength)
        
    }
}
