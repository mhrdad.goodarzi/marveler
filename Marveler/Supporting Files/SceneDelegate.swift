//
//  SceneDelegate.swift
//  Marveler
//
//  Created by Arash Goodarzi on 11/26/21.
//  Copyright © 2021 Arash Goodarzi. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else {
            return
        }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        let tabbar = TabBarController()
        window?.rootViewController = tabbar
        window?.makeKeyAndVisible()
    }
    

}

