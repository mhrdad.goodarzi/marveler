//
//  Character+CoreDataProperties.swift
//  
//
//  Created by Arash on 11/28/21.
//
//

import Foundation
import CoreData

//FYI: if i changed the file folder xcode throws error
extension Character {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Character> {
        return NSFetchRequest<Character>(entityName: "Character")
    }

    @NSManaged public var resultModel: Data?
    @NSManaged public var id: Int32

}
